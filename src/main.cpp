#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include <LittleFS.h>
#include <Bounce2.h>
#include <PubSubClient.h>

typedef struct {
  char ssid[50] = "gnejs";
  char password[50] = "";
  char mqtt_server[50] = "zig.local";
  char mqtt_prefix[50] = "postbox";
  bool enableWifi = true;
  bool enableMqtt = false;
} Prefs;

Prefs prefs;
AsyncWebServer webServer(80);
WiFiClient espClient;
PubSubClient mqttClient(espClient);
Bounce switch1 = Bounce();      // postbox in hatch switch, low active
Bounce switch2 = Bounce();      // postbox out hatch switch, low active
int wifiTriesLeft = 10;

void setStringFromArg(AsyncWebServerRequest *request, char *dest, size_t destSize, const char *argName, bool ignoreEmpty, bool &anySet);
void setBoolFromArg(AsyncWebServerRequest *request, bool &dest, const char *argName, bool ignoreEmpty, bool &anySet);
void prefs2json(Prefs &prefs, JsonDocument &jsonBuffer, boolean includePassword);
void json2prefs(JsonDocument &jsonBuffer, Prefs &prefs);
void loadPrefsFile();
void reconnect();

void setup() {
  Serial.begin(115200);
  delay(10);

  LittleFS.begin();
  loadPrefsFile();

  // try connecting to wifi
  WiFi.enableSTA(true);
  Serial.print("\nwifi connecting to "); Serial.println(prefs.ssid);
  WiFi.begin(prefs.ssid, prefs.password);
  while (WiFi.status() != WL_CONNECTED && wifiTriesLeft > 0) {
    delay(1000);
    wifiTriesLeft--;
  }
  Serial.println();
  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("wifi connected http://"); Serial.println(WiFi.localIP());
  } else {
    Serial.print("wifi connection failed");

    // log failure and chage to AP mode
    Serial.println("Changing to Access Point mode ssid postboxmqtt pw bettertoo, open: http://192.168.4.1");
    WiFi.mode(WIFI_AP);
    WiFi.softAP("postboxmqtt", "bettertoo");
  }

  if (prefs.enableMqtt) {
    mqttClient.setServer(prefs.mqtt_server, 1883);
  }

  // web service to get all prefs except password as json
  webServer.on("/prefs", HTTP_GET, [](AsyncWebServerRequest *request){
    AsyncResponseStream *response = request->beginResponseStream("text/json");
    StaticJsonDocument<200> jsonBuffer;
    prefs2json(prefs, jsonBuffer, false);
    serializeJson(jsonBuffer, *response);
    request->send(response);
  });

  // receive POSTed json, set prefs from it and save json to file prefs.json
  // or receive POSTed json, save to file and read file and set prefs from it
  webServer.on("/prefs", HTTP_POST, [](AsyncWebServerRequest *request){
    bool anySet = false;
    setStringFromArg(request, prefs.ssid, sizeof prefs.ssid, "ssid", false, anySet);
    setStringFromArg(request, prefs.password, sizeof prefs.password, "password", true, anySet);
    setStringFromArg(request, prefs.mqtt_server, sizeof prefs.mqtt_server, "mqtt_server", false, anySet);
    setStringFromArg(request, prefs.mqtt_prefix, sizeof prefs.mqtt_prefix, "mqtt_prefix", false, anySet);
    setBoolFromArg(request, prefs.enableWifi, "enableWifi", true, anySet);
    setBoolFromArg(request, prefs.enableMqtt, "enableMqtt", true, anySet);
    if (anySet) {
      // save prefs to file
      File of = LittleFS.open("/prefs.json", "w");
      StaticJsonDocument<200> jsonBuffer;
      prefs2json(prefs, jsonBuffer, true);
      serializeJson(jsonBuffer, of);
      of.close();
    }
    if (request->hasArg("form_submit")) {
      request->redirect("/");
    } else {
      request->send(200);
    }
  });

  webServer.serveStatic("/", LittleFS, "/").setDefaultFile("index.html");

  switch1.attach(D1,  INPUT_PULLUP);
  switch2.attach(D2,  INPUT_PULLUP);

  webServer.begin();
}

boolean gotPost = false;

void loop() {
  switch1.update();
  switch2.update();

  if (prefs.enableMqtt) {
    if (!mqttClient.connected()) {
      reconnect();
    }
    mqttClient.loop();
  }

  if (switch1.rose()) {
    if (!gotPost) {
      gotPost = true;
      Serial.println("got post");
      mqttClient.publish(prefs.mqtt_prefix, "true", true);
    }
  }
  if (switch2.rose()) {
    if (gotPost) {
      gotPost = false;
      Serial.println("no post");
      mqttClient.publish(prefs.mqtt_prefix, "false", true);
    }
  }
}

// if webServer has the argument argName, then copy it as a string to dest (max destSize bytes), and set anySet to true.
void setStringFromArg(AsyncWebServerRequest *request, char *dest, size_t destSize, const char *argName, bool ignoreEmpty, bool &anySet) {
  if (request->hasArg(argName)) {
    String value = request->arg(argName);
    if (!ignoreEmpty || value.length() > 0) {
      strncpy(dest, value.c_str(), destSize);
      dest[destSize - 1] = 0;
      anySet = true;
    }
  }
}

// if webServer has the argument argName, then copy it as a string to dest (max destSize bytes), and set anySet to true.
void setBoolFromArg(AsyncWebServerRequest *request, bool &dest, const char *argName, bool ignoreEmpty, bool &anySet) {
  if (request->hasArg(argName)) {
    dest = request->arg(argName).equalsIgnoreCase("true");
    anySet = true;
  }
}

void prefs2json(Prefs &prefs, JsonDocument &jsonBuffer, boolean includePassword) {
    jsonBuffer["enableWifi"] = prefs.enableWifi;
    jsonBuffer["enableMqtt"] = prefs.enableMqtt;
    jsonBuffer["ssid"] = prefs.ssid;
    if (includePassword) {
      jsonBuffer["password"] = prefs.password;
    }
    jsonBuffer["mqtt_server"] = prefs.mqtt_server;
    jsonBuffer["mqtt_prefix"] = prefs.mqtt_prefix;
}

void json2prefs(JsonDocument &jsonBuffer, Prefs &prefs) {
    prefs.enableWifi = jsonBuffer["enableWifi"];
    prefs.enableMqtt = jsonBuffer["enableMqtt"];
    strncpy(prefs.ssid, jsonBuffer["ssid"], sizeof prefs.ssid);
    strncpy(prefs.password, jsonBuffer["password"], sizeof prefs.password);
    strncpy(prefs.mqtt_server, jsonBuffer["mqtt_server"], sizeof prefs.mqtt_server);
    strncpy(prefs.mqtt_prefix, jsonBuffer["mqtt_prefix"], sizeof prefs.mqtt_prefix);
}

void loadPrefsFile() {
  File prefsFile = LittleFS.open("/prefs.json", "r");
  if (prefsFile.available()) {
    StaticJsonDocument<200> jsonBuffer;
    DeserializationError error = deserializeJson(jsonBuffer, prefsFile);
    if (error) {
      Serial.print("deser error");
    } else {
      json2prefs(jsonBuffer, prefs);
      Serial.print("\nloaded prefs file\n");
    }
  }
  prefsFile.close();
}

void reconnect() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (mqttClient.connect(clientId.c_str())) {
      Serial.println("mqtt connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}