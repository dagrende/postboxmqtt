# Postbox - MQTT notifier

I have a postbox with an in-hatch at the front and an out-hatch at the back. I want to be notified when the postman put something into the postbox.

I programmed a Wemos D1 mini to send an MQTT message on topic "postbox" with payload true when the in-hatch is opened, and  with payload false when the back-hatch is opened.

## Development environment

- Install VS Code
- Open Extensions tab and install PlatformIO IDE
- clone this repo
- cd postboxmqtt
- code .
- click the build button - the check mark at window bottom
- connect the d1 mini with a usb cable
- upload the app by clicking the right arrow butom at window bottom
- Click the PlatformIO alien to the left
  - Click Project tasks > Platform > Build Filesystem Image
  - Click Project tasks > Platform > Upload Filesystem Image

- open serial console with usb connector button at bottom
- reset d1 mini
- the serial console shopuld say that d1 mini runs as an access point
- make your PC connect to the d1 mini WiFi net postboxmqtt with password bettertoo
- open http://192.168.4.1
- fill in the form
- reset d1 mini
- the serial console should tell about connection to the wifi and the ip address
- serial should tell that d1 mini connected to the mqtt broker
